<?php
    use yii\helpers\Html;
?>
<div>
    <?= $model->titulo ?>
</div>

<div>
    <?= $model->contenido ?>
</div>

<div>
    <?= Html::a('Eliminar',
            ['site/eliminar','id' => $model->id],
            [
                'class'=>'btn btn-primary',
                // confirmacion realizada con javascript y una ventana emergente
                /*'data' => [
                    'confirm' => '¿Estas seguro que deseas eliminar el registro?',
                    'method' => 'post',
                ],*/ 
            ],
            ) 
    ?>
</div>

