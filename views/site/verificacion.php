<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    "model" => $model
]);

?>

<div>
    <?= Html::a('¿Estas seguro que deseas Eliminar este registro?',
            ['site/eliminar1','id' => $model->id],
            [
                'class'=>'btn btn-primary',
            ],
            ) 
    ?>
</div>

