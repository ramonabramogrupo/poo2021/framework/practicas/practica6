<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Notas;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $activeDataProvider = new ActiveDataProvider([
            'query' => Notas::find(),
            'pagination' => [
                'pageSize' => 2
            ],
        ]);

        return $this->render('index', [
                    "dataProvider" => $activeDataProvider,
        ]);
    }

    public function actionAnadir() {
        $model = new Notas();

        // cargando en el modelo los datos del formulario
        if ($model->load(Yii::$app->request->post())) {
            // insertando los datos en la tabla
            if ($model->save()) {
                return $this->redirect(["site/index"]);
            }
        }

        return $this->render('formulario', [
                    'model' => $model,
        ]);
    }
    
    
    public function actionEliminar($id){
        // cargando el registro a borrar
        $model= Notas::findOne($id);
        
        //llamamos a una vista para verificar el borrado
        return $this->render("verificacion",[
            "model" => $model,
        ]);
        
        // si utilizo el metodo de data para eliminar
        /*$model->delete();
        return $this->redirect(["site/index"]);*/
        
    }
    
    
    public function actionEliminar1($id){
        $model=Notas::findOne($id);
        $model->delete();
        return $this->redirect(["site/index"]);
    }

}
